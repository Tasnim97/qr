class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
  	helper_method :current_user, :get_door, :unlock, :iot_open, :iot_close
  	after_action -> { flash.discard }
  	  	
	def iot_open
  		sc = UDPSocket.new
		sc.send("o", 0, "192.168.10.76", 2800)
		sc.close
		puts "open"
  	end

  	def iot_close
  		Thread.new do
  			sleep(3)
	  		sc = UDPSocket.new
			sc.send("c", 0, "192.168.10.76", 2800)
			sc.close
			puts "close"
		end
  	end

	def authenticate
		redirect_to :login unless user_signed_in?
	end

	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end

	def get_door
		return Door.where({ id: ['1'] }).first
	end

	def unlock
		respond_to do |format|
			flash[:primary] = "Door opened for 3 seconds"
			format.js { render :file => "/me/unlock.js.erb" }
	        format.json
	        format.html { render :file => "/me/show.html.erb" } 
		end
	end

	def user_signed_in?
		# converts current_user to a boolean by negating the negation
		!!current_user
	end
end
