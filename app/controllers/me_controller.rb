class MeController < ApplicationController
	before_action :authenticate
	def show
		if params[:go] == "1"
			iot_open
			iot_close
			unlock
		end
	end
end
