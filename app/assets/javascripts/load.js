$(function() {
  $('[data-toggle="tooltip"]').tooltip()
  $(window).load(function(){
    $("div#flashdiv").delay(2000).slideUp(500, function(){
      $("div#flashdiv").alert('close');
    });
    var url = window.location.href;
    if(url.includes("?go=1")){
      window.history.replaceState( {} , '', '/me' );
    }
  });
});